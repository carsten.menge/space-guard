﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : NetworkBehaviour
{
  [SyncVar]
  private bool activated;

  public void OnInteract(PlayerController p)
  {
    if (p.role == PlayerRole.Traitor)
      RpcInteractTraitor();
    else
      RpcInteractNonTraitor();
  }

  [ClientRpc]
  void RpcInteractNonTraitor()
  {
    if (activated)
      GetComponentInChildren<SpriteRenderer>().color = Color.red;
    else
      GetComponentInChildren<SpriteRenderer>().color = Color.green;
    activated = !activated;
  }

  [ClientRpc]
  private void RpcInteractTraitor()
  {
    NetworkBehaviour.Destroy(gameObject);
  }
}