﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : NetworkBehaviour
{
  [Header("Player Variables")]
  public float Speed = 10f;

  private Machine operatingMachine;

  [SyncVar(hook = nameof(SetRole))]
  public PlayerRole role;

  private void Update()
  {
    // Don't control other players...
    if (!isLocalPlayer) return;

    var horizontal = Input.GetAxis("Horizontal") * Speed * Time.deltaTime;
    var vertical = Input.GetAxis("Vertical") * Speed * Time.deltaTime;
    transform.Translate(new Vector2(horizontal, vertical));

    if (operatingMachine != null)
    {
      if (Input.GetKeyDown(KeyCode.E)) CmdInteractWithMachine();
    }

    Camera.main.transform.parent = transform;
    Camera.main.transform.localPosition = new Vector3(0, 0, -15f);
  }
  

  // ignore collisions with other players
  private void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.tag == "Player") Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
  }

  public void OnProximityToMachine(Machine machine)
  {
    operatingMachine = machine;
  }

  public void OnLeavingMachine()
  {
    operatingMachine = null;
  }

  [Command]
  private void CmdInteractWithMachine()
  {
    operatingMachine.OnInteract(this);
  }

  private void SetRole(PlayerRole oldRole, PlayerRole newRole)
  {
    if (newRole == PlayerRole.Traitor)
      GetComponent<SpriteRenderer>().color = Color.black; 
    else
      GetComponent<SpriteRenderer>().color = Color.white;
  }
}
