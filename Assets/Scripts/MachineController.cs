﻿using UnityEngine;
using System.Collections;
using Mirror;

public class MachineController : NetworkBehaviour
{
  private void OnTriggerEnter2D(Collider2D collision)
  {
    PlayerController p;
    if (collision.GetComponent<Collider2D>().TryGetComponent<PlayerController>(out p))
      p.OnProximityToMachine(GetComponentInParent<Machine>());
  }

  private void OnTriggerExit2D(Collider2D collision)
  {
    PlayerController p;
    if (collision.GetComponent<Collider2D>().TryGetComponent<PlayerController>(out p))
      p.OnLeavingMachine();
  }
}
