﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnMessage : MessageBase
{
  public PlayerRole role;
}

public enum PlayerRole
{
  Crew, 
  CO,
  Traitor
}
